---
external: false
title: "我的歷代電腦配置"
description: ""
date: 2024-04-08
---

#### 大學
- CPU：AMD Ryzen 9 7900X
- 記憶體：DDR5-6000Mhz 16GBx2
- 主機板：GIGABYTE B650M GAMING X AX (rev. 1.3)
- GPU：AMD Radeon RX 6600 XT
- 機殼：Fractal Design Pop Air 秋日橘

#### 大學
- CPU：AMD Ryzen 5 5600X
- 記憶體：DDR4-3200Mhz 16GBx2
- 主機板：ASUS ROG STRIX B550-F GAMING (WI-FI)
- GPU：AMD Radeon RX 6600 XT
- 機殼：MONTECH 君主 Air X 黑

#### 高中
- CPU：AMD Ryzen 5 5600X
- 記憶體：DDR4-3200Mhz 16GBx2
- 主機板：ASUS ROG STRIX B550-F GAMING (WI-FI)
- GPU：NVIDIA GeForce GTX 1070
- 機殼：MONTECH 君主 Air X 黑

#### 高中
- CPU：AMD Ryzen 7 3700X
- 記憶體：DDR4-3200Mhz 16GBx2
- 主機板：ASUS ROG STRIX B550-F GAMING (WI-FI)
- GPU：AMD Radeon R7 240
- 機殼：MONTECH 君主 Air X 黑

#### 高中
- CPU：Intel Core i5-9400f
- 記憶體：DDR4-2666Mhz 8GBx2
- 主機板：ASUS B450M-DRAGON
- GPU：NVIDIA GeForce RTX 2060 SUPER
- 機殼：視博通【統治者】ATX電腦機殼《黑》

#### 國中 (人生第一台電腦)
- CPU：Intel Core i5-9400f
- 記憶體：DDR4-2666Mhz 8GBx2
- 主機板：ASUS B450M-DRAGON
- GPU：NVIDIA GeForce GTX 1060 6GB
- 機殼：視博通【統治者】ATX電腦機殼《黑》