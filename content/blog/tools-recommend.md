---
external: false
title: "電腦相關工具軟體推薦"
description: ""
date: 2024-10-09
---

- [**系統相關**](#system)
- [**硬體相關**](#hardware)
- [**驅動相關**](#driver)
- [**網路相關**](#network)
- [**週邊相關**](#input)

## System - 系統相關
- [Visual C++ Redistributable Runtimes AIO](https://github.com/abbodi1406/vcredist/releases)
- [Adguard Store - Windows UWP應用程式安裝包載點生成](https://store.rg-adguard.net/)
- [Revo Uninstaller - 程式解除安裝工具](https://www.revouninstaller.com/revo-uninstaller-free-download/)
- [LatencyMon - 系統延遲檢查](https://www.resplendence.com/latencymon)
- [Frame Latency Meter - 遊戲輸入延遲測試工具](https://gpuopen.com/flm/)

## Hardware - 硬體相關
- **硬體資訊檢查工具**
  - [HWINFO](https://www.hwinfo.com/)
  - [AIDA64](https://www.aida64.com/downloads)
  - [CPU-Z](https://www.cpuid.com/softwares/cpu-z.html)
  - [GPU-Z](https://www.techpowerup.com/gpuz/)
  - [CrystalDiskInfo - 硬碟壽命檢測](https://crystalmark.info/en/software/crystaldiskinfo/)
  - [WizTree - 硬碟內各個檔案大小以圖形化呈現](https://diskanalyzer.com/)
- **穩定性測試工具**
  - [OCCT](https://www.ocbase.com/)
  - [AIDA64](https://www.aida64.com/downloads)
  - [Furmark](https://www.geeks3d.com/furmark/)
- **跑分軟體**
  - [3DMark](https://www.3dmark.com/zh-hant/)
  - [Cinebench R23](https://apps.microsoft.com/detail/9pgzkjc81q7j?cid=majornelson&hl=zh-tw&gl=TW)
  - [Cinebench 2024](https://www.maxon.net/en/downloads/cinebench-2024-downloads)

## Driver - 驅動相關
- **驅動下載**
  - AMD GPU
    - [AMD驅動下載](https://www.amd.com/zh-hant/support)
    - [AMD第三方驅動下載](https://www.amernimezone.com/)
    - [AnWave - AMD顯卡驅動第三方客製化安裝工具](https://sourceforge.net/projects/nvidia-power-management/)
  - NVIDIA GPU
    - [NVIDIA驅動下載](https://www.nvidia.com.tw/Download/index.aspx?lang=tw)
    - [NVCleanstall - NVIDIA顯卡驅動第三方客製化安裝工具](https://www.techpowerup.com/download/techpowerup-nvcleanstall/)
- **驅動清除工具**
  - [AMD Cleanup Utility](https://www.amd.com/en/resources/support-articles/faqs/GPU-601.html)
  - [Display Driver Uninstaller (DDU)](https://www.guru3d.com/download/display-driver-uninstaller-download)

## Network - 網路相關
- [Speedtest by Ookla 網速測試](https://www.speedtest.net/)
- [Cloudflare Internet Speed Test 網速測試](https://speed.cloudflare.com/)
- [IPv6 測試](https://test-ipv6.com/)

## Input - 週邊相關
- **滑鼠**
  - [滑鼠連點測試](https://codepen.io/blink172/pen/vERyxK)
  - [滑鼠滾輪測試](https://fractalglider.github.io/fun/2018/02/13/testing-mouse-scroll-wheel.html)
  - [RAZER 滑鼠輪詢率測試軟體下載](https://rzr.to/pollingrate)
- **麥克風**
  - [Equalizer APO](https://sourceforge.net/projects/equalizerapo/)