---
external: false
title: "決勝時刻：現代戰爭 II 最佳設定"
description: ""
date: 2024-05-27
---

![MWII Benchmark](/images/mwii_bench.webp)

#### 教學影片
- [Youtube](https://youtu.be/jZ_WEX4ZCOU?si=f_OPH7fEHr_wD_ua)
- [Bilibili](https://b23.tv/cAXZZ5K)

#### 設定檔下載
- [下載](https://gitlab.com/TN-TechNoob/tn-technoob.gitlab.io/-/raw/main/static/Game-Configs/MWII/options.3.cod22.cst?ref_type=heads&inline=false)