---
external: false
title: "自己個人最推薦的驅動安裝方式"
description: ""
date: 2024-04-11
---

又是一個長篇大論，沒辦法，這年代文章不夠長，人們不願相信

一般來講你可以讓Windows Update直接自動搜尋所有驅動並安裝，這確實也是大部分小白無意間灌驅動的方式，但是因為一些原因(可能是微軟的鍋，也可能是廠商們的鍋)導致Windows Update幫你安裝的驅動都是相對較舊的，在部分情況下舊的驅動就有可能會導致系統或軟體在運行上出現非預期的情況(系統藍屏，遊戲或軟體崩潰等等...)，所以我自己個人最推薦的驅動安裝方式就是先禁用Windows Update的自動搜尋並安裝驅動功能(注意禁用這個功能只會讓Windows Update禁用搜尋並安裝驅動並不會禁用系統品質更新，所以請你放心)

**禁用Windows Update的自動搜尋並安裝驅動功能**
- [註冊檔下載(.reg)](https://gitlab.com/TN-TechNoob/tn-technoob.gitlab.io/-/raw/main/static/Disable-Automatic-Driver-Installation/Disable%20Automatic%20Driver%20Installation.reg?inline=false)

禁用完之後呢再手動安裝自己電腦所需的驅動程式，驅動程式推薦的下載位置如下

**晶片組驅動及顯卡驅動**
- 廠商官網
    - [Intel](https://www.intel.com.tw/content/www/tw/zh/download-center/home.html)
    - [AMD](https://www.amd.com/support)
    - [NVIDIA](https://www.nvidia.com.tw/Download/index.aspx?lang=tw)

**其餘驅動(主板音效驅動、無線及有線網卡及藍牙驅動)**
- 主板官網

很多觀眾遇到電腦問題跑來找我求救，我的處理方式其實就是上面那樣必要的話頂多再加個更新Bios並調調設定，就這樣的處理流程解決了不知道多少人的電腦問題像是遊戲閃退、系統報kernal自動重開機、遊戲間歇性卡頓及遊戲效能表現不如預期(低FPS)等等

好了，大概就是這樣，希望這個文章能幫助到你們，覺得有用的話也可以幫我分享出去