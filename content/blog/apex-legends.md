---
external: false
title: "Apex Legends 最佳設定"
description: ""
date: 2024-06-30
---

{% youtube url="https://www.youtube.com/embed/5gQB6u7aAyY?si=gtCEF32K5bNHS8Lh" label="Apex Legends - 第18賽季 - 究級效能圖形設定檔，大幅提升FPS" /%}

#### 啟動項
```
+fps_max 0 -novid -anticheat_settings=SettingsDX12.json
```

#### 設定檔下載
- [下載](https://gitlab.com/TN-TechNoob/tn-technoob.gitlab.io/-/raw/main/static/Game-Configs/Apex/videoconfig.txt?ref_type=heads&inline=false)
