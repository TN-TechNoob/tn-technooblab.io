---
external: false
title: "GTA V 最佳設定"
description: ""
date: 2024-08-04
---

{% youtube url="https://www.youtube.com/embed/ZVmx3TQmaec?si=vJZRRhxoElfKkeIX" label="GTA V - 究級效能圖形設定檔，大幅提升FPS" /%}

#### 教學影片
- [Youtube](https://youtu.be/ZVmx3TQmaec)
- [Bilibili](https://b23.tv/PFEN1Np)

#### 設定檔下載
- [下載](https://gitlab.com/TN-TechNoob/tn-technoob.gitlab.io/-/raw/main/static/Game-Configs/GTA%20V/settings.xml?ref_type=heads&inline=false)